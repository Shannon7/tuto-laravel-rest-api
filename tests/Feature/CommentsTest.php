<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetCommentShouldReturnAComment()
    {
        $EXPECTED_CODE_FRAGMENT = ['text'=> 'commentaire #1'];

        $reponse = $this->JSON('GET', '/api/comments');

        $reponse->assertJsonFragment($EXPECTED_CODE_FRAGMENT);
    }

    public function testGetCommentShouldReturnAValidStatus()
    {
        $EXPECTED_CODE_STATUS = 200;

        $reponse = $this->JSON('GET', '/api/comments');

        $reponse->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testPostShouldReturnANewComment()
    {
        $EXPECTED_CODE_FRAGMENT = ['user_id'=>1, 'text'=>'Voici mon nouveau commentaire'];

        $reponse = $this->JSON('POST', '/api/comments', $EXPECTED_CODE_FRAGMENT);

        $reponse->assertJsonFragment($EXPECTED_CODE_FRAGMENT);
    }

    public function testPostShouldReturnA201()
    {
        $CODE_FRAGMENT = ['user_id'=>1, 'text'=>'Voici mon nouveau commentaire'];
        $EXPECTED_CODE_STATUS = 201;

        $reponse = $this->JSON('POST', '/api/comments', $CODE_FRAGMENT);

        $reponse->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testPostShouldReturnA422WhenTextFieldEmpty()
    {
        $CODE_FRAGMENT = ['text'=>''];
        $EXPECTED_CODE_STATUS = 422;

        $reponse = $this->JSON('POST', '/api/comments', $CODE_FRAGMENT);

        $reponse->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testPostShouldReturnAErrorMessageWhenTextFieldEmpty()
    {
        $CODE_FRAGMENT = ['text'=>''];
        $EXPECTED_CODE_FRAGMENT = ['The text field is required.'];

        $reponse = $this->JSON('POST', '/api/comments', $CODE_FRAGMENT);

        $reponse->assertJsonFragment($EXPECTED_CODE_FRAGMENT);
    }
}
