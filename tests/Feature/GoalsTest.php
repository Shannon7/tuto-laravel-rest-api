<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;

class GoalsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetGoalsShouldReturnAGoal()
    {
        $EXPECTED_CODE_FRAGMENT = ['description' => 'description #10',
            'title' => 'title #10',
            'points' => 10,
            'categories_id' => 1,
            'published_at' => date("Y-m-d")
        ];

        $reponse = $this->JSON('GET', '/api/goals');

        $reponse->assertJsonFragment($EXPECTED_CODE_FRAGMENT);
    }

    public function testGetGoalsShouldReturnAValidStatus()
    {
        $EXPECTED_CODE_STATUS = 200;

        $reponse = $this->JSON('GET', '/api/goals');

        $reponse->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testGetGoalsWithIdShouldReturnAGoal()
    {
        $EXPECTED_CODE_FRAGMENT = ['description' => 'description #10',
            'title' => 'title #10',
            'points' => 10,
            'categories_id' => 1,
            'published_at' => date("Y-m-d")
        ];

        $reponse = $this->JSON('GET', '/api/goals/10');

        $reponse->assertJsonFragment($EXPECTED_CODE_FRAGMENT);
    }

    public function testGetGoalsWithIdShouldReturnAValidStatus()
    {
        $EXPECTED_CODE_STATUS = 200;

        $reponse = $this->JSON('GET', '/api/goals/10');

        $reponse->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testGetGoalsWithWrongIdShouldReturnStatusCode404()
    {
        $EXPECTED_CODE_STATUS = 404;

        $reponse = $this->JSON('GET', '/api/goals/1000');

        $reponse->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testGetGoalsWithCategoryShouldReturnStatusCode200()
    {
        $EXPECTED_CODE_STATUS = 200;

        $reponse = $this->JSON('GET', '/api/goals/category/1');

        $reponse->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testGetGoalsWithCategoryShouldReturnGoalsOfTheCategory()
    {
        $EXPECTED_CODE_FRAGMENT = ['description' => 'description #10',
            'title' => 'title #10',
            'points' => 10,
            'categories_id' => 1,
            'published_at' => date("Y-m-d")
        ];

        $reponse = $this->JSON('GET', '/api/goals/category/1');

        $reponse->assertJsonFragment($EXPECTED_CODE_FRAGMENT);
    }

    public function testGetGoalsWithWrongCategoryShouldReturnStatusCode404()
    {
        $EXPECTED_CODE_STATUS = 404;

        $reponse = $this->JSON('GET', '/api/goals/category/1000');

        $reponse->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testPostShouldReturnANewGoal()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        $EXPECTED_CODE_FRAGMENT = ['description' => 'description #101',
            'title' => 'title #101',
            'points' => 101,
            'categories_id' => 1,
            'published_at' => date("Y-m-d")
        ];

        $reponse = $this->post('/api/goals/101', $EXPECTED_CODE_FRAGMENT);

        $reponse->assertJsonFragment($EXPECTED_CODE_FRAGMENT);
    }

    public function testPostShouldReturnA201()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        $EXPECTED_CODE_STATUS = 201;

        $CODE_FRAGMENT = ['description' => 'description #101',
            'title' => 'title #101',
            'points' => 101,
            'categories_id' => 1,
            'published_at' => date("Y-m-d")
        ];

        $reponse = $this->post('/api/goals/101', $CODE_FRAGMENT);

        $reponse->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testPostWithInvalidUserShouldReturnA403()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        $EXPECTED_CODE_STATUS = 403;

        $CODE_FRAGMENT = ['description' => 'description #101',
            'title' => 'title #101',
            'points' => 101,
            'categories_id' => 1,
            'published_at' => date("Y-m-d")
        ];

        $reponse = $this->post('/api/goals/101', $CODE_FRAGMENT);

        $reponse->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testPutGoalsShouldReturnStatus200()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        $EXPECTED_CODE_STATUS = 200;

        $CODE_FRAGMENT = ['description' => 'description #101',
            'title' => 'title #101',
            'points' => 101,
            'categories_id' => 1,
            'published_at' => date("Y-m-d")
        ];

        $response = $this->JSON('PUT',
                                '/api/goals/1',
                                $CODE_FRAGMENT);

        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testPutGoalsWithInvalidUserShouldReturnA403()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        $EXPECTED_CODE_STATUS = 403;

        $CODE_FRAGMENT = ['description' => 'description #101',
            'title' => 'title #101',
            'points' => 101,
            'categories_id' => 1,
            'published_at' => date("Y-m-d")
        ];

        $reponse = $this->JSON('PUT',
                                '/api/goals/1',
                                $CODE_FRAGMENT);

        $reponse->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testPutGoalsWithoutTitleShouldReturn422()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        $EXPECTED_CODE_STATUS = 422;

        $CODE_FRAGMENT = ['description' => 'description #101',
            'points' => 101,
            'categories_id' => 1,
            'published_at' => date("Y-m-d")
        ];

        $response = $this->JSON('PUT',
                                '/api/goals/1',
                                $CODE_FRAGMENT);

        $response->assertJsonFragment(['title' => ['The title field is required.']]);
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testPutGoalsWithoutDescriptionShouldReturn422()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        $EXPECTED_CODE_STATUS = 422;

        $CODE_FRAGMENT = ['title' => 'title #101',
            'points' => 101,
            'categories_id' => 1,
            'published_at' => date("Y-m-d")
        ];

        $response = $this->JSON('PUT',
                                '/api/goals/1',
                                $CODE_FRAGMENT);

        $response->assertJsonFragment(['description' => ['The description field is required.']]);
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testPutGoalsWithoutPointsShouldReturn422()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        $EXPECTED_CODE_STATUS = 422;

        $CODE_FRAGMENT = ['description' => 'description #101',
            'title' => 'title #101',
            'categories_id' => 1,
            'published_at' => date("Y-m-d")
        ];

        $response = $this->JSON('PUT',
                                '/api/goals/1',
                                $CODE_FRAGMENT);

        $response->assertJsonFragment(['points' => ['The points field is required.']]);
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testPutGoalsWithoutCategoriesIdShouldReturn422()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        $EXPECTED_CODE_STATUS = 422;

        $CODE_FRAGMENT = ['description' => 'description #101',
            'title' => 'title #101',
            'points' => 101,
            'published_at' => date("Y-m-d")
        ];

        $response = $this->JSON('PUT',
                                '/api/goals/1',
                                $CODE_FRAGMENT);

        $response->assertJsonFragment(['categories_id' => ['The categories id field is required.']]);
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testPutGoalsWithoutPublishedAtShouldReturn422()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        $EXPECTED_CODE_STATUS = 422;

        $CODE_FRAGMENT = ['description' => 'description #101',
            'title' => 'title #101',
            'points' => 101,
            'categories_id' => 1
        ];

        $response = $this->JSON('PUT',
                                '/api/goals/1',
                                $CODE_FRAGMENT);

        $response->assertJsonFragment(['published_at' => ['The published at field is required.']]);
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testPutGoalsWithWrongIdAtShouldReturn404()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        $EXPECTED_CODE_STATUS = 404;

        $CODE_FRAGMENT = ['description' => 'description #101',
            'title' => 'title #101',
            'points' => 101,
            'categories_id' => 1,
            'published_at' => date("Y-m-d")
        ];

        $response = $this->JSON('PUT',
                                '/api/goals/1000',
                                $CODE_FRAGMENT);

        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testDeleteGoalsWithWrongIdAtShouldReturn404()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        $EXPECTED_CODE_STATUS = 404;

        $response = $this->JSON('DELETE','/api/goals/1000');
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testDeleteGoalsWithUnauthorizedUserShouldReturn403()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        $EXPECTED_CODE_STATUS = 403;

        $response = $this->JSON('DELETE','/api/goals/1');
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testDeleteGoalsWithAuthorizedUserShouldReturn200()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        $EXPECTED_CODE_STATUS = 200;

        $response = $this->JSON('DELETE','/api/goals/34');
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testAddGoalToUserWithUserShouldReturn200()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        $CODE_FRAGMENT = [
            'user_id' => 2,
            'goals_id' => 3,
            'states_id' => 1
        ];

        $EXPECTED_CODE_STATUS = 200;

        $response = $this->JSON('POST','/api/me/goals/3', $CODE_FRAGMENT);
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testAddGoalToUserNotLogInShouldReturn401()
    {
        $CODE_FRAGMENT = [
            'user_id' => 1,
            'goals_id' => 2,
            'states_id' => 1
        ];

        $EXPECTED_CODE_STATUS = 401;

        $response = $this->JSON('POST','/api/me/goals/2', $CODE_FRAGMENT);
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testAddGoalToUserWithAdminShouldReturn200()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        $CODE_FRAGMENT = [
            'user_id' => 1,
            'goals_id' => 2,
            'states_id' => 1
        ];

        $EXPECTED_CODE_STATUS = 200;

        $response = $this->JSON('POST','/api/me/goals/2', $CODE_FRAGMENT);
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testAddGoalToUserWithGoalThatDoesNotExistShouldReturn404()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        $CODE_FRAGMENT = [
            'user_id' => 1,
            'goals_id' => 2,
            'states_id' => 1
        ];

        $EXPECTED_CODE_STATUS = 404;

        $response = $this->JSON('POST','/api/me/goals/6942042', $CODE_FRAGMENT);
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testCompletedGoalWithUserShouldReturn200()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        $CODE_FRAGMENT = [
            'user_id' => 1,
            'goals_id' => 1,
            'states_id' => 2
        ];

        $EXPECTED_CODE_STATUS = 200;

        $response = $this->JSON('POST','/api/me/goals/1', $CODE_FRAGMENT);
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testGetGoalsOfUserShouldReturnCodeStatus200()
    {

        $EXPECTED_CODE_STATUS = 200;

        $response = $this->JSON('GET','/api/user/1/goals/');
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testGetGoalsOfInvalidUserShouldReturnCodeStatus404()
    {

        $EXPECTED_CODE_STATUS = 404;

        $response = $this->JSON('GET','/api/user/2000/goals/');
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testGetGoalsOfUserShouldReturnGoals()
    {

        $EXPECTED_CODE_FRAGMENT = ['description' => 'description #10',
            'title' => 'title #10',
            'points' => 10,
            'categories_id' => 1,
            'published_at' => date("Y-m-d")
        ];

        $response = $this->JSON('GET','/api/user/10/goals/');
        $response->assertJsonFragment($EXPECTED_CODE_FRAGMENT);
    }

    public function testGetLeaderBoardShouldReturn200()
    {

        $EXPECTED_CODE_STATUS = 200;


        $response = $this->JSON('GET','/api/leaderboard/');
        $response->assertStatus($EXPECTED_CODE_STATUS);
    }

    public function testGetLeaderBoardShouldReturnAUser()
    {

        $EXPECTED_CODE_FRAGMENT = [
        'id'=> 1
        ];

        $response = $this->JSON('GET','/api/leaderboard/');
        $response->assertJsonFragment($EXPECTED_CODE_FRAGMENT);
    }
}
