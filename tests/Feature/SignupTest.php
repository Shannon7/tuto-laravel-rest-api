<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SignupTest extends TestCase
{

    use DatabaseTransactions;
    
    const DEFAULT_NAME = 'default';
    const DEFAULT_EMAIL = 'default@email.com';
    const DEFAULT_PASSWORD = 'default';
    const DEFAULT_TOKEN_HEADER = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6I';

    const DEFAULT_UNAUTH_EMAIL = 'unauth@email.com';
    const DEFAULT_UNAUTH_HEADER = 'unauth';


    public function testPostSignupTest()
    {
        $reponse = $this->post('/api/register',
                            ['name' => self::DEFAULT_NAME,
                             'email' => self::DEFAULT_EMAIL,
                             'password' => self::DEFAULT_PASSWORD]);

        $reponse->assertSeeText("token");
        $reponse->assertSeeText(self::DEFAULT_TOKEN_HEADER);
        $reponse->assertStatus(200);
    }

    public function testPostSinginTestShouldReturn200()
    {
        $reponse = $this->post('/api/login',
                            ['email' => 'admin@api.com',
                             'password' => 'admin']);

        $reponse->assertStatus(200);
    }

    public function testPostSigninInvalidUserTestShouldReturn401()
    {
        $reponse = $this->post('/api/login',
                            ['username' => self::DEFAULT_EMAIL,
                             'password' => self::DEFAULT_PASSWORD]);

        $reponse->assertStatus(401);
    }
}
