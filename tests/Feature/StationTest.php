<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;

class StationTest extends TestCase
{
    use DatabaseTransactions;

    public function testPostStation()
    {
        // Arrange
        Passport::actingAs(
            \App\User::find(1)
        );
        
        // Act
        $response = $this->JSON('POST',
                                '/api/stations',
                                ['name' => 'Quebec Station',
                                 'description' => 'Old City station',
                                 'long' => -70.8925,
                                 'lat' => 44.4856,
                                 'user_id' => 1]);
        // Assert
        $response->assertJsonFragment(['name' => 'Quebec Station',
                                       'description' => 'Old City station',
                                       'long' => -70.8925,
                                       'lat' => 44.4856]);
        $response->assertStatus(201);
    }

    public function testPostStationWithoutAuthentication()
    {
        $response = $this->JSON('POST',
                                '/api/stations',
                                 ['name' => 'Quebec Station',
                                  'description' => 'Old City station',
                                  'long' => -70.8925,
                                  'lat' => 44.4856,
                                  'user_id' => 1]);

        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
        $response->assertStatus(401);
    }

    public function testPostStationWithoutDescriptionShouldWork()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('POST',
                                '/api/stations',
                                ['name' => 'Quebec Station',
                                 'long' => -70.8925,
                                 'lat' => 44.4856,
                                 'user_id' => 1]);

        $response->assertJsonFragment(['name' => 'Quebec Station',
                                       'long' => -70.8925,
                                       'lat' => 44.4856]);
        $response->assertStatus(201);
    }

    public function testPostStationWithoutName()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('POST',
                                '/api/stations',
                                 ['description' => 'Old City station',
                                  'long' => -70.8925,
                                  'lat' => 44.4856,
                                  'user_id' => 1]);

        $response->assertJsonFragment(['name' => ['The name field is required.']]);
        $response->assertStatus(422);
    }

    public function testPostStationWithoutLong()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('POST',
                                '/api/stations',
                                ['name' => 'Quebec Station',
                                 'description' => 'Old City station',
                                 'lat' => 44.4856,
                                 'user_id' => 1]);

        $response->assertJsonFragment(['long' => ['The long field is required.']]);
        $response->assertStatus(422);
    }

    public function testPostStationWithoutLat()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('POST',
                                '/api/stations',
                                ['name' => 'Quebec Station',
                                 'description' => 'Old City station',
                                 'long' => -70.8925,
                                 'user_id' => 1]);

        $response->assertJsonFragment(['lat' => ['The lat field is required.']]);
        $response->assertStatus(422);
    }

    public function testPostStationWithStringLong()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('POST',
                                '/api/stations',
                                ['name' => 'Quebec Station',
                                 'description' => 'Old City station',
                                 'long' => 'string',
                                 'lat' => 44.4856,
                                 'user_id' => 1]);
        $response->assertJsonFragment(['long' => ['The long must be a number.']]);
        $response->assertStatus(422);
    }

    public function testPostStationWithStringLat()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('POST',
                                '/api/stations',
                                ['name' => 'Quebec Station',
                                 'description' => 'Old City station',
                                 'long' => -70.8925,
                                 'lat' => 'string',
                                 'user_id' => 1]);
        $response->assertJsonFragment(['lat' => ['The lat must be a number.']]);
        $response->assertStatus(422);
    }

    public function testGetStationsIndex()
    {
        $response = $this->JSON('GET',
                                '/api/stations');
        $response->assertStatus(200);
    }

    public function testGetStationsFromAUserDeterminedOnRoute()
    {
        $response = $this->JSON('GET',
                                '/api/users/2/stations');

        $response->assertJsonFragment(['name' => 'Cégep de Ste-Foy station',
                                        'description' => 'G-266 station',
                                        'long' => -71.29,
                                        'lat' => 46.78,
                                        'user_id' => 2]);
        $response->assertStatus(200);
    }

    public function testGetStationsFromAUserDeterminedOnRouteUserHasManyStations()
    {
        $response = $this->JSON('GET',
            '/api/users/1/stations');

        $response->assertJsonFragment(['name' => '924 Gilman station',
                                        'long' => -122.30,
                                        'lat' => 37.88,
                                        'user_id' => 1]);

        $response->assertJsonFragment(['name' => 'Broomfield station',
                                        'description' => 'CO2 station',
                                        'long' => -3.11,
                                        'lat' => 51.10,
                                        'user_id' => 1]);
        $response->assertStatus(200);
    }

    public function testGetStationsFromAUserWhoHasNoStations()
    {
        $response = $this->JSON('GET',
            '/api/users/3/stations');

        $response->assertJsonFragment(['message' => 'User does not have any stations.']);
        $response->assertStatus(404);
    }

    public function testGetStationsFromAUserWhoDoesNotExist()
    {
        $response = $this->JSON('GET',
            '/api/users/-1/stations');

        $response->assertStatus(404);
    }

    public function testGetStationFromAStationId()
    {
        $response = $this->JSON('GET',
                                '/api/stations/3');

        $response->assertJsonFragment(['name' => 'Cégep de Ste-Foy station',
                                       'description' => 'G-266 station',
                                       'long' => -71.29,
                                       'lat' => 46.78,
                                       'user_id' => 2]);
        $response->assertStatus(200);
    }

    public function testGetStationsFromAnInvalidStationId()
    {
        $response = $this->JSON('GET',
            '/api/stations/-1');

        $response->assertStatus(404);
    }

    public function testPutStationWithRightValues()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('PUT',
                                '/api/stations/1',
                                ['name' => 'Quebec Station',
                                 'description' => 'Old City station',
                                 'long' => -70.8925,
                                 'lat' => 44.4856]);

        $response->assertJsonFragment(['name' => 'Quebec Station',
            'description' => 'Old City station',
            'long' => -70.8925,
            'lat' => 44.4856]);
        $response->assertStatus(200);
    }

    public function testPutStationWithoutAuthentication()
    {
        $response = $this->JSON('PUT',
                                '/api/stations/1',
                                ['name' => 'Quebec Station',
                                 'description' => 'Old City station',
                                 'long' => -70.8925,
                                 'lat' => 44.4856]);

        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
        $response->assertStatus(401);
    }

    public function testPutStationAsWrongUser()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('PUT',
                                '/api/stations/3',
                                ['name' => 'Quebec Station',
                                 'description' => 'Old City station',
                                 'long' => -70.8925,
                                 'lat' => 44.4856]);

        $response->assertJsonFragment(['message' => 'Unauthorized action.']);
        $response->assertStatus(403);
    }

    public function testPutStationWithoutDescriptionShouldWork()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('PUT',
                                '/api/stations/1',
                                ['name' => 'Quebec Station',
                                 'long' => -70.8925,
                                 'lat' => 44.4856]);

        $response->assertJsonFragment(['name' => 'Quebec Station',
                                       'long' => -70.8925,
                                       'lat' => 44.4856]);
        $response->assertStatus(200);
    }

    public function testPutStationWithoutName()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('PUT',
                                '/api/stations/1',
                                ['description' => 'Old City station',
                                 'long' => -70.8925,
                                 'lat' => 44.4856]);

        $response->assertJsonFragment(['name' => ['The name field is required.']]);
        $response->assertStatus(422);
    }

    public function testPutStationWithoutLong()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('PUT',
                                '/api/stations/1',
                                ['name' => 'Quebec Station',
                                 'description' => 'Old City station',
                                 'lat' => 44.4856]);

        $response->assertJsonFragment(['long' => ['The long field is required.']]);
        $response->assertStatus(422);
    }

    public function testPutStationWithoutLat()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('PUT',
                                '/api/stations/1',
                                ['name' => 'Quebec Station',
                                 'description' => 'Old City station',
                                 'long' => -70.8925]);

        $response->assertJsonFragment(['lat' => ['The lat field is required.']]);
        $response->assertStatus(422);
    }

    public function testPutStationWithStringLong()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('PUT',
                                '/api/stations/1',
                                ['name' => 'Quebec Station',
                                 'description' => 'Old City station',
                                 'long' => 'string',
                                 'lat' => 44.4856]);
        $response->assertJsonFragment(['long' => ['The long must be a number.']]);
        $response->assertStatus(422);
    }

    public function testPutStationWithStringLat()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('PUT',
                                '/api/stations/1',
                                ['name' => 'Quebec Station',
                                 'description' => 'Old City station',
                                 'long' => -70.8925,
                                 'lat' => 'string']);
        $response->assertJsonFragment(['lat' => ['The lat must be a number.']]);
        $response->assertStatus(422);
    }

    public function testDeleteStationWithInvalidId()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('DELETE','/api/stations/-1');
        $response->assertStatus(404);
    }

    public function testDeleteStationWithValidIdShouldRemoveSpecifiedStation()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $newStation = $this->JSON('POST',
                                '/api/stations',
                                ['name' => 'Quebec Station',
                                 'description' => 'Old City station',
                                 'long' => -70.8925,
                                 'lat' => 44.4856,
                                 'user_id' => 1]);
        $newId = json_decode($newStation->getContent())->data->id;

        $response = $this->JSON('DELETE', '/api/stations/'.$newId);
        $response->assertStatus(200);

        //Station should not be found now
        $response = $this->JSON('GET','/api/stations/'.$newId);
        $response->assertStatus(404);
    }

    public function testDeleteStationWithoutAuthentication()
    {
        $response = $this->JSON('DELETE', '/api/stations/1');
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
        $response->assertStatus(401);
    }

    public function testDeleteStationWithoutWrongUser()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->JSON('DELETE', '/api/stations/3');
        $response->assertJsonFragment(['message' => 'Unauthorized action.']);
        $response->assertStatus(403);
    }
}
