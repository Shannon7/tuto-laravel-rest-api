<?php

namespace Tests\Feature;

use Tests\TestCase;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MeasureTest extends TestCase
{

    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPostMeasureTest()
    {
        // Arrange
        Passport::actingAs(
            \App\User::find(1)
        );

        // Act
        $response = $this->post('/api/stations/1/measures',
                                ['value'=> '3', 'description'=>'co2'],
                                ['Accept'=> 'application/json']);
        
        // Assert
        $response->assertJsonFragment(['value'=>'3']);
        $response->assertStatus(201);
    }

    public function testPostMeasureWithoutValueTest()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->post('/api/stations/1/measures',
                                ['description'=>'co2'],
                                ['Accept'=> 'application/json']);
        $response->assertJsonFragment(["value"=>["The value field is required."]]);
        $response->assertStatus(422);
    }

    public function testPostMeasureWithoutDescriptionTest()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->post('/api/stations/1/measures',
                                ['value' => '25'],
                                ['Accept' => 'application/json']);
        $response->assertJsonFragment(["description" => ["The description field is required."]]);
        $response->assertStatus(422);
    }

    public function testPostMeasureWithStringValueFieldTest()
    {
        Passport::actingAs(
            \App\User::find(1)
        );
        $response = $this->post('/api/stations/1/measures',
                                ['value' => 'invalid input', 'description' => 'co2'],
                                ['Accept' => 'application/json']);
        $response->assertJsonFragment(["value" => ["The value must be an integer."]]);
        $response->assertStatus(422);
    }

    public function testPostMeasureOnStationNotOwned()
    {
        Passport::actingAs(
            \App\User::find(2)
        );
        $response = $this->post('/api/stations/1/measures',
                                    ['value'=> '3', 'description'=>'co2'],
                                    ['Accept'=> 'application/json']);
        $response->assertJsonFragment(['message'=>'Unauthorized action.']);
        $response->assertStatus(403);
    }

    public function testGetMeasureWithInexistingStationIdTest()
    {
        $response = $this->get('/api/stations/-1/measures');
        $response->assertStatus(404);
    }

    public function testGetMeasureWithValidIndexReturnsGoodMeasure()
    {
        $response = $this->get('/api/stations/1/measures');
        $response->assertJsonFragment(["id" => 1, "value" => 73, "description" => "co2", "station_id" => 1]);
        $response->assertJsonFragment(["id" => 2, "value" => 30, "description" => "nh4", "station_id" => 1]);
        $response->assertJsonFragment(["id" => 3, "value" => 8, "description" => "nox", "station_id" => 1]);
        $response->assertStatus(200);
    }

    public function testGetMeasureFromStationWithoutMeasures()
    {
        $response = $this->get('/api/stations/3/measures');
        $response->assertJsonFragment(["message" => "Station has no measure."]);
        $response->assertStatus(200);
    }

    public function testGet24hFromStationWithNoMeasuresInLast24h()
    {
        //Measures in seeder does not have a creation date
        $response = $this->get('/api/stations/1/measures/24h');
        $response->assertJsonFragment(["message" => "Station has no measure."]);
        $response->assertStatus(200);
    }

    public function testGet24hFromStationWithMeasures()
    {
        Passport::actingAs(
            \App\User::find(1)
        );

        //Add two measures
        $this->post('/api/stations/1/measures',
                        ['value'=> 4, 'description'=>'nox'],
                        ['Accept'=> 'application/json']);
        $this->post('/api/stations/1/measures',
                        ['value'=> 3, 'description'=>'co2'],
                        ['Accept'=> 'application/json']);

        //The two measures should be returned
        $response = $this->get('/api/stations/1/measures/24h');
        $response->assertJsonFragment(["value" => 3, "description" => "nox", "station_id" => 1]);
        $response->assertJsonFragment(["value" => 3, "description" => "co2", "station_id" => 1]);
        $response->assertStatus(200);
    }

    public function testGet24hOnInexistingStation()
    {
        $response = $this->get('/api/stations/-1/measures/24h');
        $response->assertStatus(404);
    }

    public function testGetMeasureShouldReturnIndexColorAndLabelWhenValueIsLessThan50()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        //Add measure in station 3, which has no measure.
        $this->post('/api/stations/3/measures',
                    ['value'=> 4, 'description'=>'nox'],
                    ['Accept'=> 'application/json']);

        $response = $this->get('/api/stations/3/measures', ['Accept' => 'application/json']);
        $response->assertJsonFragment(['indice' => ['color' => 'Green', 'label' => 'Good']]);
        $response->assertStatus(200);
    }

    public function testGetMeasureShouldReturnIndexColorAndLabelWhenValueIs50()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        //Add measure in station 3, which has no measure.
        $this->post('/api/stations/3/measures',
            ['value'=> 50, 'description'=>'nox'],
            ['Accept'=> 'application/json']);

        $response = $this->get('/api/stations/3/measures', ['Accept' => 'application/json']);
        $response->assertJsonFragment(['indice' => ['color' => 'Green', 'label' => 'Good']]);
        $response->assertStatus(200);
    }

    public function testGetMeasureShouldReturnIndexColorAndLabelWhenValueIsBetween50and100()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        //Add measure in station 3, which has no measure.
        $this->post('/api/stations/3/measures',
            ['value'=> 73, 'description'=>'nox'],
            ['Accept'=> 'application/json']);

        $response = $this->get('/api/stations/3/measures', ['Accept' => 'application/json']);
        $response->assertJsonFragment(['indice' => ['color' => 'Yellow', 'label' => 'Average']]);
        $response->assertStatus(200);
    }

    public function testGetMeasureShouldReturnIndexColorAndLabelWhenValueIs100()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        //Add measure in station 3, which has no measure.
        $this->post('/api/stations/3/measures',
            ['value'=> 100, 'description'=>'nox'],
            ['Accept'=> 'application/json']);

        $response = $this->get('/api/stations/3/measures', ['Accept' => 'application/json']);
        $response->assertJsonFragment(['indice' => ['color' => 'Yellow', 'label' => 'Average']]);
        $response->assertStatus(200);
    }

    public function testGetMeasureShouldReturnIndexColorAndLabelWhenValueIsBetween100and150()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        //Add measure in station 3, which has no measure.
        $this->post('/api/stations/3/measures',
            ['value'=> 126, 'description'=>'nox'],
            ['Accept'=> 'application/json']);

        $response = $this->get('/api/stations/3/measures', ['Accept' => 'application/json']);
        $response->assertJsonFragment(['indice' => ['color' => 'Orange', 'label' => 'Bad for sensible people']]);
        $response->assertStatus(200);
    }

    public function testGetMeasureShouldReturnIndexColorAndLabelWhenValueIs150()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        //Add measure in station 3, which has no measure.
        $this->post('/api/stations/3/measures',
            ['value'=> 150, 'description'=>'nox'],
            ['Accept'=> 'application/json']);

        $response = $this->get('/api/stations/3/measures', ['Accept' => 'application/json']);
        $response->assertJsonFragment(['indice' => ['color' => 'Orange', 'label' => 'Bad for sensible people']]);
        $response->assertStatus(200);
    }

    public function testGetMeasureShouldReturnIndexColorAndLabelWhenValueIsBetween150and200()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        //Add measure in station 3, which has no measure.
        $this->post('/api/stations/3/measures',
            ['value'=> 178, 'description'=>'nox'],
            ['Accept'=> 'application/json']);

        $response = $this->get('/api/stations/3/measures', ['Accept' => 'application/json']);
        $response->assertJsonFragment(['indice' => ['color' => 'Red', 'label' => 'Bad']]);
        $response->assertStatus(200);
    }

    public function testGetMeasureShouldReturnIndexColorAndLabelWhenValueIs200()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        //Add measure in station 3, which has no measure.
        $this->post('/api/stations/3/measures',
            ['value'=> 200, 'description'=>'nox'],
            ['Accept'=> 'application/json']);

        $response = $this->get('/api/stations/3/measures', ['Accept' => 'application/json']);
        $response->assertJsonFragment(['indice' => ['color' => 'Red', 'label' => 'Bad']]);
        $response->assertStatus(200);
    }

    public function testGetMeasureShouldReturnIndexColorAndLabelWhenValueIsBetween200and300()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        //Add measure in station 3, which has no measure.
        $this->post('/api/stations/3/measures',
            ['value'=> 250, 'description'=>'nox'],
            ['Accept'=> 'application/json']);

        $response = $this->get('/api/stations/3/measures', ['Accept' => 'application/json']);
        $response->assertJsonFragment(['indice' => ['color' => 'Purple', 'label' => 'Very bad']]);
        $response->assertStatus(200);
    }

    public function testGetMeasureShouldReturnIndexColorAndLabelWhenValueIs300()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        //Add measure in station 3, which has no measure.
        $this->post('/api/stations/3/measures',
            ['value'=> 300, 'description'=>'nox'],
            ['Accept'=> 'application/json']);

        $response = $this->get('/api/stations/3/measures', ['Accept' => 'application/json']);
        $response->assertJsonFragment(['indice' => ['color' => 'Purple', 'label' => 'Very bad']]);
        $response->assertStatus(200);
    }

    public function testGetMeasureShouldReturnIndexColorAndLabelWhenValueIsMoreThan300()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        //Add measure in station 3, which has no measure.
        $this->post('/api/stations/3/measures',
            ['value'=> 333, 'description'=>'nox'],
            ['Accept'=> 'application/json']);

        $response = $this->get('/api/stations/3/measures', ['Accept' => 'application/json']);
        $response->assertJsonFragment(['indice' => ['color' => 'Brown', 'label' => 'Dangerous']]);
        $response->assertStatus(200);
    }
}
