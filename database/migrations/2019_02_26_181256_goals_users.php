<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GoalsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goals_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index();
            $table->integer('goals_id')->unsigned()->index();
            $table->integer('states_id')->unsigned()->default(App\States::$STARTED)->index();
            $table->primary(['user_id', 'goals_id']);
            $table->timestamps();
        });

        Schema::table('goals_user', function (Blueprint $table) {

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('goals_id')->references('id')->on('goals')->onDelete('cascade');
            $table->foreign('states_id')->references('id')->on('states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goals_user');
    }
}
