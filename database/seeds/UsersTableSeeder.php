<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@api.com',
            'password' => Hash::make('admin'),
            'role' => 'Admin'
        ]);

        DB::table('users')->insert([
            'name' => 'Jesse Michaels',
            'email' => 'opiv_commonRider@gmail.com',
            'password' => Hash::make('healthyBodySickMind')
        ]);

        DB::table('users')->insert([
            'name' => 'Joe Strummer',
            'email' => 'joe_strummer@outlook.com',
            'password' => Hash::make('legend77')
        ]);

        DB::table('users')->insert([
            'name' => 'Tim Armstrong',
            'email' => 'tim_timebomb@rancid.com',
            'password' => Hash::make('rubySoho')
        ]);

        DB::table('users')->insert([
            'name' => 'Axl Rose',
            'email' => 'axlrose@gunsnroses.com',
            'password' => Hash::make('novemberRain')
        ]);
    }
}
