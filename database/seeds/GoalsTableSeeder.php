<?php

use Illuminate\Database\Seeder;

class GoalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 50; $i++){
            DB::table('goals')->insert(
                [
                    'description' => 'description #' . $i,
                    'title' => 'title #' . $i,
                    'points' => $i,
                    'categories_id' => 1,
                    'published_at' => date("Y/m/d")
            ]);
        }

        for($i = 1; $i <= 50; $i++){
            DB::table('goals')->insert(
                [
                    'description' => 'description #' . $i . $i,
                    'title' => 'title #' . $i . $i,
                    'points' => $i . $i,
                    'categories_id' => 2,
                    'published_at' => date("Y/m/d")
            ]);
        }
    }
}
