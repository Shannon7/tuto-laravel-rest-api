<?php

use Illuminate\Database\Seeder;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert(
            [
                'name' => 'state #' . App\States::$STARTED
            ]);

        DB::table('states')->insert(
            [
                'name' => 'state #' . App\States::$COMPLETED
            ]);

        DB::table('states')->insert(
            [
                'name' => 'state #' . App\States::$CANCELLED
            ]);
    }
}
