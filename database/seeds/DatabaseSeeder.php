<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesSeederTable::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProfilesTableSeeder::class);
        $this->call(StationSeeder::class);
        $this->call(MeasureTableSeeder::class);
        $this->call(CommentsSeeder::class);
        $this->call(GoalsTableSeeder::class);
        $this->call(StatesSeeder::class);
        $this->call(GoalsUsersSeeder::class);
    }
}
