<?php

use Illuminate\Database\Seeder;

class GoalsUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 5; $i++){
            DB::table('goals_user')->insert(
                [
                    'user_id' => $i,
                    'goals_id' => $i,
                    'states_id' => 1
                ]);
        }
    }
}
