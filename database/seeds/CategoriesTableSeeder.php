<?php

use Illuminate\Database\Seeder;

class CategoriesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 3; $i++){
            DB::table('categories')->insert(
                [
                    'name' => 'name #' . $i,
            ]);
        }
    }
}
