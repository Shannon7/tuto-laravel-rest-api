<?php

use Illuminate\Database\Seeder;

class CommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 50; $i++){
            DB::table('comments')->insert(
                [
                    'user_id' => 1,
                    'text' => 'commentaire #' . $i
            ]);
        }
    }
}
