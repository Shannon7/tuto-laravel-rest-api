<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    static public $STARTED = 1;
    static public $COMPLETED = 2;
    static public $CANCELLED = 3;
}
