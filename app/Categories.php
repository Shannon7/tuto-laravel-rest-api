<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    public function goals()
    {
        return $this->hasMany('App\Goals')->withTimestamps();
    }
}
