<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GoalsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'description' => $this->description,
            'title' => $this->title,
            'points' => $this->points,
            'created_at' => $this->created_at,
            'published_at' => $this->published_at,
            'categories_id' => $this->categories_id,
            'id' => $this->id,
        ];
    }
}
