<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\States;
use App\Goals;
use App\User;
use App\Categories;
use App\Http\Resources\GoalsResource;
use App\Http\Resources\UserResource;
use App\Http\Requests\GoalsPostRequest;



class GoalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return GoalsResource::collection(Goals::paginate());
    }

    public function showGoalsById(int $id)
    {
        $goals = Goals::findOrFail($id);
        return new GoalsResource($goals);
    }

    public function showGoalsByCategoryId($id)
    {
        $categories = Categories::findOrFail($id);
        $goals = GoalsResource::collection(Goals::all());
        $key = 1;
        $array = array();
        foreach($goals as $goal){
            if($goal->categories_id == $categories->id){
                $array = array_add($array, $key, new GoalsResource($goal));
                $key++;
            }
        }
        return $array;
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GoalsPostRequest $request)
    {
        $goals = new Goals();
        $goals->title = $request->input('title');
        $goals->description = $request->input('description');
        $goals->points = $request->input('points');
        $goals->categories_id = $request->input('categories_id');
        $goals->published_at = $request->input('published_at');

        $goals->save();

        return new GoalsResource($goals);
    }

    public function AddGoalToUser(Request $request, $id)
    {
        $states_id = $request->states_id;
        $states = States::findOrFail($states_id);
        $user_id = $request->user_id;
        $user = User::findOrFail($user_id);
        $goals = Goals::findOrFail($id);

        if($states_id == 1){
            $user->goals()->attach($id);
        }
        else {
            $user->pivot->goals_id;
            $user->pivot->save();
        }

        $user->save();
    }


    public function GetCompletedGoalsOfUser($id)
    {
        $user = User::findOrFail($id);
        $arrayOfGoals = $user->completedGoals();
        return $arrayOfGoals;
    }


    public function GetLeaderBoard()
    {
        $allUser = User::all();
        $key = 1;
        $arrayOfUser = array();
        foreach($allUser as $user){
            $arrayOfUser = array_add($arrayOfUser, $key, new UserResource($user));
        }
        $this->bubble_sort($arrayOfUser);
        return $arrayOfUser;
    }

    
    function bubble_sort($arr) {
        $n = count($arr);
        do {
            $swapped = false;
            for ($i = 0; $i < $n - 1; $i++) {
                if ($arr[$i]->getPoints() > $arr[$i + 1]->getPoints()) {
                    $temp = $arr[$i];
                    $arr[$i] = $arr[$i + 1];
                    $arr[$i + 1] = $temp;
                    $swapped = true;
                }
            }
            $n--;
        }
        while ($swapped);
        return $arr;
    }
    

/*    public function CompletedGoal(Request $request, $id)
    {
        $goals = Goals::findOrFail($id);
        $goals->states_id = 2;
        $user->save();
    }

    public function CancelledGoal(Request $request, $id)
    {
        $user_id = $request->user_id;
        $user = User::findOrFail($user_id);
        $goals = Goals::findOrFail($id);
        $user->goals()->attach($goals);
        $user->save();
    }*/

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GoalsPostRequest $request, $id)
    {
        $goals = Goals::findOrFail($id);

        $goals->title = $request->input('title');
        $goals->description = $request->input('description');
        $goals->points = $request->input('points');
        $goals->categories_id = $request->input('categories_id');
        $goals->published_at = $request->input('published_at');
        
        $goals->save();

        return new GoalsResource($goals);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $goals = Goals::findOrFail($id);

        if ($goals->delete()) {
            return new GoalsResource($goals);
        }
    }
}
