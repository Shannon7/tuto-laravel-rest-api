<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Goals extends Model
{
    public function user()
    {
        return $this->belongsToMany('App\User', 'goals_user')->withTimestamps()->withPivot('user_id');
    }

    public function categories()
    {
        return $this->belongsTo('App\Categories')->withTimestamps();
    }
}
