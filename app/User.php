<?php

namespace App;

use App\Goals_user;
use App\States;
use App\Http\Resources\Goals_UserResource;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;


    public static $ROLE_ADMIN = 'admin';
    public static $ROLE_USER = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $with = ['profile'];

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function comments()
    {
        return $this->hasMany('App\Comments');
    }

    public function stations()
    {
        return $this->hasMany('App\Station');
    }
    
    public function goals()
    {
        return $this->belongsToMany('App\Goals', 'goals_user')->withTimestamps()->withPivot('goals_id', 'states_id');
    }

    public function completedGoals()
    {
        $goals_user = Goals_UserResource::collection(Goals_user::all());
        $key = 1;
        $array = array();
        foreach($goals_user as $goal){
            if($goal->state_id == States::$COMPLETED){
                $array = array_add($array, $key, new GoalsResource(Goals::findOrFail($goal->goals_id)));
                $key++;
            }
        }
        return $array;
    }


    public function getPoints()
    {
        $allCompletedGoals = $this->completedGoals();
        $totalPoints = 0;
        foreach($allCompletedGoals as $goal){
            $totalPoints += $goal->points;
        }
    }
}
